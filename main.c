#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define RED 1
#define BLACK 0
#define MAX_PALAVRA 50


//Criação da árvore ArvoreRB
//char parr, com o limite de 50 caracteres.
typedef struct arvoreRB {
    char parr[MAX_PALAVRA];
    int qtdOcorrencias;
    int cor;
    struct arvoreRB *esq;
    struct arvoreRB *dir;
} ArvoreRB;

struct arquivinho {
    char* nome;
    double TFidf;
    struct arquivinho *pr;
} typedef arquivinho;

typedef struct repetePalv {
    ArvoreRB* palavra;
    struct repetePalv *p;
} RepetePalv;

//Verifica se o nó é vermelho e retorna nó vermelho caso o anterior for preto, se não tiver nó
// na iteração anterior retorna preto
int eh_no_vermelho (ArvoreRB *no) {
    if(!no) return BLACK;
    return(no->cor == RED);
}

//Insere nó
ArvoreRB* novo_no (char* palavra) {
    ArvoreRB *arv = (ArvoreRB*) malloc(sizeof(ArvoreRB));
    strcpy(arv->parr, palavra);
    arv->esq = arv->dir = NULL;
    arv->cor = RED;
    arv->qtdOcorrencias = 1;
    return arv;
}

//Insere folhas
ArvoreRB* direcao_dir(ArvoreRB* dir) {
    ArvoreRB *esq = dir->esq;
    dir->esq = esq->dir;
    esq->dir = dir;
    esq->cor = dir->cor;
    dir->cor = RED;
    return (esq);
}

ArvoreRB* direcao_esq(ArvoreRB* esq) {
    ArvoreRB *dir = esq->dir;
    esq->dir = dir->esq;
    dir->esq = esq;
    dir->cor = esq->cor;
    esq->cor = RED;
    return (dir);
}

//Inverte cores dos nós quando precisa
void inverte_cor (ArvoreRB *arv) {
    arv->cor = !(arv->cor);
    arv->esq->cor = !(arv->esq->cor);
    arv->dir->cor = !(arv->dir->cor);
}

ArvoreRB* insere_ArvoreRB (ArvoreRB *arv, char* palavra) {
    if (arv == NULL) return novo_no(palavra);
    if (strcmp(palavra, arv->parr) < 0)
        arv->esq = insere_ArvoreRB(arv->esq, palavra);
    else if (strcmp(palavra, arv->parr) > 0)
        arv->dir = insere_ArvoreRB(arv->dir, palavra);
    else {
        arv->qtdOcorrencias++;
        return arv;
    }

    if (eh_no_vermelho(arv->dir) && !eh_no_vermelho(arv->esq)) {
        arv = direcao_esq(arv);
    }
    if (eh_no_vermelho(arv->esq) && eh_no_vermelho(arv->esq->esq)) {
        arv = direcao_dir(arv);
    } else if (eh_no_vermelho(arv->esq) && eh_no_vermelho(arv->dir)) {
        inverte_cor(arv);
    }
    return arv;
}


void print(ArvoreRB *arv, int espaco) {
    if (arv == NULL)
        return;
    int i;
    espaco += 10;
    print(arv->dir, espaco);
    for(i=10;i<espaco;i++) printf(" ");
    if(arv->cor) printf(" ");
    printf("%s %d\n", arv->parr, arv->qtdOcorrencias);
    print(arv->esq, espaco);
}

//Seleciona a inserção do arquivo na árvore rb
//arruma palavra com letras maiúsculas, compostas, ignora com menos de 2 caracteres e tals
ArvoreRB* arquivoArvoreRB(ArvoreRB* arv, int* result, char* arq) {
    int tr = 0;
    (*result) = 0;
    char palavra[MAX_PALAVRA];
    *palavra = '\0';
    char buffer[1024];
    FILE *file = fopen(arq, "r");

    if (!file) {
        printf("Erro ao abrir o arquivo %s.\n", arq);
        return arv;
    }

    while (!feof(file)) {
        size_t bytesRead = fread(buffer, 1, sizeof(buffer), file);

        for (size_t i = 0; i < bytesRead; ++i) {
            if (buffer[i] >= 'A' && buffer[i] <= 'Z') {
                buffer[i] += ' ';
            }
            if (buffer[i] >= 'a' && buffer[i] <= 'z') {
                tr = 1;
                strncat(palavra, &(buffer[i]), 1);
            } else {
                if (tr && strlen(palavra) > 2) {
                    (*result)++;
                    arv = insere_ArvoreRB(arv, palavra);
                    arv->cor = BLACK;
                }
                *palavra = '\0';
                tr = 0;
            }
        }
    }

    if (tr && strlen(palavra) > 2) {
        (*result)++;
        arv = insere_ArvoreRB(arv, palavra);
        arv->cor = BLACK;
    }

    fclose(file);
    return arv;
}

//coisas do heap 
struct heap {
    arquivinho* dados;
    int tamanho;
    int capacidade;
};

// cria um novo heap
struct heap* criarHeap(int capacidade) {
    struct heap* h = (struct heap*)malloc(sizeof(struct heap));
    h->dados = (arquivinho*)malloc(capacidade * sizeof(arquivinho));
    h->tamanho = 0;
    h->capacidade = capacidade;
    return h;
}

// troca dois elementos no heap
void trocar(arquivinho* a, arquivinho* b) {
    arquivinho temp = *a;
    *a = *b;
    *b = temp;
}

void ajustarHeap(struct heap* h, int indice) {
    int maior = indice;
    int esq = 2 * indice + 1;
    int dir = 2 * indice + 2;

    if (esq < h->tamanho && h->dados[esq].TFidf > h->dados[maior].TFidf)
        maior = esq;

    if (dir < h->tamanho && h->dados[dir].TFidf > h->dados[maior].TFidf)
        maior = dir;

    if (maior != indice) {
        trocar(&(h->dados[indice]), &(h->dados[maior]));
        ajustarHeap(h, maior);
    }
}

void inserirHeap(struct heap* h, arquivinho elemento) {
    if (h->tamanho == h->capacidade) {
        printf("Heap cheio. Não é possível adicionar mais elementos.\n");
        return;
    }

    h->tamanho++;
    int indice = h->tamanho - 1;
    h->dados[indice] = elemento;

    while (indice != 0 && h->dados[(indice - 1) / 2].TFidf < h->dados[indice].TFidf) {
        trocar(&(h->dados[indice]), &(h->dados[(indice - 1) / 2]));
        indice = (indice - 1) / 2;
    }
}

// extrair o elemento máximo
arquivinho extrairMaximo(struct heap* h) {
    if (h->tamanho <= 0) {
        printf("Heap vazio.\n");
        arquivinho elementoVazio;
        elementoVazio.nome = NULL;
        elementoVazio.TFidf = -1.0;
        elementoVazio.pr = NULL;
        return elementoVazio;
    }
    if (h->tamanho == 1) {
        h->tamanho--;
        return h->dados[0];
    }

    arquivinho raiz = h->dados[0];
    h->dados[0] = h->dados[h->tamanho - 1];
    h->tamanho--;
    ajustarHeap(h, 0);

    return raiz;
}

void liberarHeap(struct heap* h) {
    free(h->dados);
    free(h);
}


//Função que que vê a frequencia das palavras do .txt
//(coloquei aqui pq se deixar depois do search o código simplesmente não funciona)
int freqPalavra (ArvoreRB* arv, char* palav) {
    if (arv == NULL) return 0;
    if (!strcmp(arv->parr, palav)) return arv->qtdOcorrencias;

    int f = freqPalavra(arv->esq, palav);
    if (f) return f;
    f = freqPalavra(arv->dir, palav);
    if (f) return f;
}


// Função de busca
void search(int argc, char** argv) {
    int docnum = argc - 3;
    ArvoreRB** arvLista = (ArvoreRB**)malloc(sizeof(ArvoreRB*) * docnum);
    int* listat = (int*)malloc(sizeof(int) * docnum);
    struct heap* maxHeap = criarHeap(docnum); // Criação do heap

    // Cálculo de relevância do texto para um termo de busca (professor pediu)
    double* tf = (double*)malloc(sizeof(double) * docnum);
    double* tfidf = (double*)malloc(sizeof(double) * docnum);
    int qtdPalavras = 0;

    for (int i = 0; i < docnum; i++) {
        arvLista[i] = NULL;
        arvLista[i] = arquivoArvoreRB(arvLista[i], &(listat[i]), argv[i + 3]);
        if (arvLista[i] == NULL) {
            printf("Parâmetro inválido!");
            return;
        }

        double TFtotal = 0;
        char palavras[MAX_PALAVRA];
        strcpy(palavras, argv[2]);
        char* pl = strtok(palavras, " ");
        int j = 0;
        while (pl != NULL) {
            int f = freqPalavra(arvLista[i], pl);
            TFtotal += (f / 100.0) / (listat[i] / 100.0);
            pl = strtok(NULL, " ");
            j++;
        }
        tf[i] = TFtotal / j;
        if (tf[i] > 0) qtdPalavras++;

        // Insere no heap
        arquivinho elemento;
        elemento.nome = argv[i + 3];
        elemento.TFidf = tf[i] * log10((docnum / 100.0) / (qtdPalavras / 100.0)) + 0.001;
        inserirHeap(maxHeap, elemento);
    }

    // extrai do heap e printa isso ae
    printf("Lista dos documentos mais relevantes para o termo \"%s\": \n", argv[2]);
    for (int i = 0; i < docnum; i++) {
        arquivinho elemento = extrairMaximo(maxHeap);
        printf("%d  %s\n", i + 1, elemento.nome);
    }

    // Libera da memória
    liberarHeap(maxHeap);
}

//Manipulação das palavras dos arquivos

//Função que pega as palavras mais repetidas
void repetePalavra(RepetePalv** lista, ArvoreRB* arv, int num) {
    if (arv == NULL) return;
    repetePalavra(lista, arv->esq, num);

    int i = 0;
    RepetePalv *at = *lista;
    RepetePalv *ant = *lista;
    while (at != NULL && arv->qtdOcorrencias <= at->palavra->qtdOcorrencias) {
        ant = at;
        at = at->p;
        i++;
    }
    if (i < num) {
        RepetePalv *el = (RepetePalv*)malloc(sizeof(RepetePalv));
        el->palavra = arv;
        if (at == *lista) {
            el->p = *lista;
            *lista = el;
        } else {
            el->p = ant->p;
            ant->p = el;
        }
        while (i < num-2 && at != NULL) {
            at = at->p;
            i++;
        }
        if (at != NULL) at->p = NULL;
    }
    repetePalavra(lista, arv->dir, num);
}

RepetePalv** repetePalavras (ArvoreRB* arv, int num) {
    RepetePalv** lista = (RepetePalv**)malloc(sizeof(RepetePalv*));
    RepetePalv* el = (RepetePalv*)malloc(sizeof(RepetePalv));
    el->palavra = arv;
    el->p = NULL;
    *lista = el;
    repetePalavra(lista, arv, num);
    return lista;
}

void liberarArvoreRB(ArvoreRB *arv) {
    if (arv == NULL) return;
    liberarArvoreRB(arv->esq);
    liberarArvoreRB(arv->dir);
    free(arv);
}

void liberarListaRepetePalv(RepetePalv *lista) {
    RepetePalv *atual = lista;
    while (atual != NULL) {
        RepetePalv *proximo = atual->p;
        free(atual);
        atual = proximo;
    }
}


int main(int argc, char** argv) {
    if (argc >= 4) {
        // Exibe o número de ocorrência das N palavras que mais aparecem em ARQUIVO, em ordem decrescente de ocorrência.
        if (strcmp(argv[1], "--freq") == 0) {
            ArvoreRB *arv = NULL;
            int t = 0;
            arv = arquivoArvoreRB(arv, &t, argv[3]);

            int num = atoi(argv[2]);
            if (num > 0) {
                RepetePalv** lista = repetePalavras(arv, num);
                printf("Palavras mais frequentes no arquivo: \n");
                RepetePalv* el = *lista;
                for (int i = 1; el != NULL; i++) {
                    printf("%d - %s (%d)\n", i, el->palavra->parr, el->palavra->qtdOcorrencias);
                    el = el->p;
                }
                liberarListaRepetePalv(*lista);
            } else {
                printf("Parâmetros inválidos!");
            }
            liberarArvoreRB(arv);
        }

        // Exibe o número de ocorrências de PALAVRA em ARQUIVO.
        else if (strcmp(argv[1], "--freq-word") == 0) {
            ArvoreRB* arv = NULL;
            int t = 0;
            arv = arquivoArvoreRB(arv, &t, argv[3]);

            if (strlen(argv[2]) > 2) {
                int f = freqPalavra(arv, argv[2]);
                printf("Quantidade de vezes que a palavra \"%s\" aparece: %d\n", argv[2], f);
            } else {
                printf("Parâmetros inválidos!");
            }
            liberarArvoreRB(arv);
        }

        // Exibe uma listagem dos ARQUIVOS mais relevantes encontrados pela busca por TERMO.
        else if (strcmp(argv[1], "--search") == 0) {
            search(argc, argv);
        }

    } else {
        printf("Parâmetros inválidos!");
    }

    return 0;
}

